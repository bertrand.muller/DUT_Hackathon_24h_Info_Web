<?php

require_once "vendor/autoload.php";

session_start();
if (!isset($_SESSION['connected']))
    $_SESSION['connected'] = false;

use app\model;
use app\controller as Controller;
use conf\ConnectionFactory as ConnectionFactory;
use \Slim\Slim;

ConnectionFactory::setConfig('db.conf.ini');

$db = ConnectionFactory::makeConnection();

$userController = new Controller\UserController();
$adminController = new Controller\AdminController();

$app = new slim();

$app->get('/', function ()/* use ($controlleurApp)*/ {
    $control = new Controller\MainController();
    $control->getHome();
});


$app->get('/home', function ()/* use ($controlleurApp)*/ {
    $control = new Controller\MainController();
    $control->getHome();
});

$app->get('/signin', function () use ($userController) {
    $userController->getSignInForm();
});

$app->post('/signin', function () use ($userController, $app) {
    $userController->postSignIn($app->request->post());
});

$app->get('/login', function () use ($userController) {
    $userController->getLoginForm();
});

$app->post('/login', function () use ($userController, $app) {
    $userController->postLogin($app->request->post());
});

$app->get('/logout', function () use ($userController) {
    $userController->logOut();
});

$app->get('/creation', function () use ($adminController) {
    $adminController->getCreationParty();
});

$app->post('/creation', function () use ($adminController, $app) {
    $adminController->postCreation($app->request->post());
});

$app->get('/applications', function () use ($adminController) {
    $adminController->getApplications();
});

$app->get('/parties', function () use ($userController) {
    $userController->getParties();
});

$app->get('/myParties', function () use ($adminController) {
    $adminController->getMyParties();
});

$app->post('/vote', function () use ($userController) {
    $userController->vote();
});

$app->post('/refuse', function () use ($adminController) {
    $adminController->rejectUser();
});

$app->get('/test', function () use ($userController) {
    $userController->DEBUG();
});

$app->get('/accept', function () use ($adminController) {
    $adminController->checkAndValidateUser();
});

$app->post('/accept', function () use ($adminController, $app) {
    $post = $app->request->post();
    if(isset($post['validate']) || isset($post['refuse'])) {
        if(isset($post['validate'])) {
            $adminController->acceptUser($post['validate']);
        }
        else {
            $adminController->rejectUser($post['refuse']);
        }
        $adminController->checkAndValidateUser();
    }
});
/*
$app->get('/voyage', function() use ($controlleurApp) {
	$controlleurApp->estimation();
});

$app->post('/voyage/estimer', function() use ($controlleurEsti,$app) {
	$controlleurEsti->estimer($app->request->post());
});

$app->get('/contact', function() use ($controlleurApp) {
	$controlleurApp->contact();
});

$app->get('/inscription', function() use ($controlleurUser) {
	$controlleurUser->genererFormInscription();
});

$app->post('/inscrire', function() use ($controlleurUser) {
	$controlleurUser->inscrireUtilisateur();
});

$app->get('/connexion', function() use ($controlleurUser) {
	$controlleurUser->genererFormConnexion();
});

$app->post('/connecter', function() use ($controlleurUser) {
	$controlleurUser->connecterUtilisateur();
});

$app->get('/deconnexion', function() use ($controlleurUser) {
	$controlleurUser->deconnecterUtilisateur();
});

$app->get('/compte', function() use ($controlleurUser) {
	$controlleurUser->afficherCompte();
});

$app->post('/compte', function() use ($controlleurUser) {
	$controlleurUser->modifierEtAfficherCompte();
});

$app->get('/enregistrer/:id', function($id) use ($controlleurEsti) {
	$controlleurEsti->enregistrerEstimationClient($id);
});

$app->get('/compte/estimations', function() use ($controlleurUser) {
	$controlleurUser->afficherEstimations();
})->name('/compte/estimations');

$app->get('/compte/estimations/suppression/:id', function($id) use ($controlleurUser) {
	$controlleurUser->supprimerEstimation($id);
});

$app->get('/compte/estimation/:id', function($id) use ($controlleurUser) {
	$controlleurUser->afficherEstimation($id);
});

$app->get('/compte/avis/:id', function($id) use ($controlleurAvis) {
	$controlleurAvis->donnerAvis($id);
});

$app->post('/compte/avis/soumettre', function() use ($controlleurAvis, $app) {
	$controlleurAvis->soumettreAvis($app->request->post());
});

$app->get('/compte/partage/:id', function($id) use ($controlleurUser) {
	$controlleurUser->partagerEstimation($id);
});

$app->get('/partage/:id', function($id) use ($controlleurApp) {
	$controlleurApp->partage($id, true);
});

$app->get('/estimation/partage/:id', function($id) use ($controlleurApp) {
	$controlleurApp->partageEstimation($id);
});

$app->get('/avis/filtrer', function() use ($controlleurAvis) {
	$controlleurAvis->filtrerAvis();
});

$app->post('/avis/liste', function() use ($controlleurAvis, $app) {
	$controlleurAvis->afficherListeAvis($app->request->post());
});

$app->get('/avis/consulter/:id', function($id) use ($controlleurApp) {
	$controlleurApp->partage($id, false);
});

$app->post('/voyage/maj/:id', function($id) use ($controlleurUser) {
	$controlleurUser->mettreAJourEstimation($id);
});
*/
$app->notFound(function () {
    $control = new Controller\MainController();
    $control->getNotFound();
});

$app->run();
