-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 21, 2016 at 05:23 
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `24h`
--

-- --------------------------------------------------------

--
-- Table structure for table `Configuration`
--

CREATE TABLE `Configuration` (
  `id` int(10) NOT NULL,
  `partyId` int(10) NOT NULL,
  `StartVoteTime` datetime NOT NULL,
  `EndVoteTime` datetime NOT NULL,
  `description` text NOT NULL,
  `NumbersOfVotes` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Configuration`
--

INSERT INTO `Configuration` (`id`, `partyId`, `StartVoteTime`, `EndVoteTime`, `description`, `NumbersOfVotes`) VALUES
(1, 1, '2016-05-21 00:00:00', '2016-05-27 00:00:00', 'Prog', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Event`
--

CREATE TABLE `Event` (
  `id` int(10) NOT NULL,
  `configurationId` int(10) NOT NULL,
  `type` text NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `location` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Event`
--

INSERT INTO `Event` (`id`, `configurationId`, `type`, `StartDate`, `EndDate`, `location`) VALUES
(1, 1, 'Repas', '2016-05-20 00:00:00', '2016-05-21 00:00:00', 'IUT Bordeaux');

-- --------------------------------------------------------

--
-- Table structure for table `Party`
--

CREATE TABLE `Party` (
  `id` int(10) NOT NULL,
  `date` date NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Party`
--

INSERT INTO `Party` (`id`, `date`, `name`) VALUES
(1, '2016-05-20', '24h DUT INFO');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `id` int(10) NOT NULL,
  `email` text NOT NULL,
  `name` text NOT NULL,
  `surname` text NOT NULL,
  `password` text NOT NULL,
  `privilege` int(4) NOT NULL,
  `avatar` text NOT NULL,
  `accepted` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `email`, `name`, `surname`, `password`, `privilege`, `avatar`, `accepted`) VALUES
(1, 'admin@admin.admin', 'admin', 'admin', '$2y$10$b0EVKLofOrQ0HZ08ImHlru22aC8.dRqVxJ6Zjgil3m3KECJq327Qm', 15, '', 1),
(2, 'user@user.user', 'user', 'user', '$2y$10$wcIgRjkIFORvWxV9ehBGsOST06jlcxdKAeHceESJA9S2jEd8fqNyy', 5, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Vote`
--

CREATE TABLE `Vote` (
  `id` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `configurationId` int(10) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Configuration`
--
ALTER TABLE `Configuration`
  ADD PRIMARY KEY (`id`),
  ADD KEY `partyId` (`partyId`);

--
-- Indexes for table `Event`
--
ALTER TABLE `Event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `configurationId` (`configurationId`);

--
-- Indexes for table `Party`
--
ALTER TABLE `Party`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Vote`
--
ALTER TABLE `Vote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `configurationId` (`configurationId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Configuration`
--
ALTER TABLE `Configuration`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Event`
--
ALTER TABLE `Event`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Party`
--
ALTER TABLE `Party`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Vote`
--
ALTER TABLE `Vote`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Configuration`
--
ALTER TABLE `Configuration`
  ADD CONSTRAINT `fk_partyId_configuration` FOREIGN KEY (`partyId`) REFERENCES `Party` (`id`);

--
-- Constraints for table `Event`
--
ALTER TABLE `Event`
  ADD CONSTRAINT `fk_configurationId_events` FOREIGN KEY (`configurationId`) REFERENCES `Configuration` (`id`);

--
-- Constraints for table `Vote`
--
ALTER TABLE `Vote`
  ADD CONSTRAINT `fk_configurationId_vote` FOREIGN KEY (`configurationId`) REFERENCES `Configuration` (`id`),
  ADD CONSTRAINT `fk_userId_vote` FOREIGN KEY (`userId`) REFERENCES `User` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
