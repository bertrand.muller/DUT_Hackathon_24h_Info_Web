<?php

namespace app\view;

class HomeView extends AbstractView {

	private $message;

	public function __construct($m) {
		$this->message = $m;
	}

	public function render($selector)
	{
		switch ($selector) {
			case 1:
				echo $this->home();
				break;
			case 2:
				echo $this->notFound();
				break;
			default:
				echo $this->notFound();
				break;
		}
	}

	public function home() {

		$app = \Slim\Slim::getInstance();
		$url = $app->request->getRootUri();
		$url = str_replace("/index.php", "", $url);
		$connect = '';

		if (isset($_SESSION['connected'])) {
			if(!$_SESSION['connected']) {
				$connect = '<a href=\"login\" id=\"download-button\" class=\"btn-large waves-effect waves-light red text-darken-1\">Connectez-vous</a>';
			}
		}

		$html = "<div id=\"index-banner\" class=\"parallax-container\">
				<div class=\"section no-pad-bot\">
					<div class=\"container\">";

		$html .= "<br><br>
						<h1 class=\"header center white-text text-darken-4 \"><b>IUT Montreal Events</b></h1>
			
						<div class=\"row center\">
							<h4 class=\"header col s12 white-text text-lighten-1\">Change your night !</h4>
						</div>
						<br><br>
			
					</div>
				</div>
				<div class=\"parallax\"><img src=\"" . $url . "/web/img/bar.jpg\"></div>
			</div>
			<div class=\"container\">
					<div class=\"section\">
				
					  <!--   Icon Section   -->
					  <div class=\"row\">
						<div class=\"col s12 m4\">
						  <div class=\"icon-block\">
							<h2 class=\"center red-text text-darken-1\"><i class=\"material-icons\">surround_sound</i></h2>
							<h5 class=\"center\">Have fun in<br>amazing parties</h5>
				
							<p class=\"light center-align\">Vous êtes membre du BDE et vous adorez faire la fête ? Vous participez à la création de moments uniques ? Venez découvrir Montréal à travers ses restaurants et ses bars ! La ville regorge de lieux adaptés au monde de la nuit...</p>
						  </div>
						</div>
				
						<div class=\"col s12 m4\">
						  <div class=\"icon-block\">
							<h2 class=\"center red-text text-darken-1\"><i class=\"material-icons\">person_pin</i></h2>
							<h5 class=\"center\">Meet new people</h5>
				
							<p class=\"light center-align\"><br>Participez à la création d'événements organisés par l'association dont vous êtes membre afin de faire de nouvelles rencontres ! Par son rayonnement multiculturel, vous pouvez faire la connaissance de personnes venant du monde entier !</p>						  </div>
						</div>
				
						<div class=\"col s12 m4\">
						  <div class=\"icon-block\">
							<h2 class=\"center red-text text-darken-1\"><i class=\"material-icons\">location_on</i></h2>
							<h5 class=\"center\">Discover a new city</h5><br>
				
							<p class=\"light center-align\">Plongée dans la nuit, la ville révéle toute sa beauté ! C'est une occasion unique de découvrir la ville sous une autre facette... Laissez-vous guider par l'ambiance de chacune de ses rues. Profitez !</p>
						  </div>
						</div>
					  </div>
				
					</div>
				  </div>
				
				  <div class=\"parallax-container valign-wrapper\">
					<div class=\"section no-pad-bot\">
					  <div class=\"container\">
						<div class=\"row center\">
						  <h4 class=\"header col s12 white-text\">Partagez un moment convivial</h5>
						</div>
					  </div>
					</div>
					<div class=\"parallax\"><img src=\"web/img/mixer.jpg\" width=1000 alt=\"Unsplashed background img 2\"></div>
				  </div>
				
				  <div class=\"container\">
					<div class=\"section\">
				
					  <div class=\"row\">
						<div class=\"col s6 offset-s3 center\">
						  <h3><i class=\"material-icons medium red-text text-darken-1 \">stay_current_portrait</i></h3>
						  <h4>Le BDE vous suit partout</h4>
						  <p class=\"center-align light\">Suivez le déroulement des prestations sur votre mobile. <br>N'hésitez pas à consulter régulièrement notre site pour voir les toutes dernières nouveautés !</p>
						</div>
					  </div>
				
					</div>
				  </div>
				
				  <div class=\"parallax-container valign-wrapper\">
				
					<div class=\"section no-pad-bot\">
				
					  <div class=\"container\">
						<div class=\"row center\">
				
							 $connect
						</div>
					  </div>
					</div>
					<div class=\"parallax\"><img src=\"web/img/restaurants.jpg\" alt=\"Unsplashed background img 3\"></div>
				  </div>";

		if($this->message != null) {
			$html .= '<script>
						document.addEventListener("DOMContentLoaded",function() {
							Materialize.toast("' . $this->message['message'] . '", 4000);
						});
					  </script>';
		}

		return $html;
	}

	public function notFound() {
		echo "404 NOT FOUND";
	}

}