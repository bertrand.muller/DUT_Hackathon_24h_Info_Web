<?php

namespace app\view;
use conf as Conf;

use app\controller as Controller;

class AdminView extends AbstractView {

	private $parties;

	public function __construct($part = null) {
		$this->parties = $part;
	}

	public function render($selector)
	{
		switch ($selector) {
			case 1:
				$this->displayFormCreationParty();
				break;
			case 2:
				$this->displayApplications();
				break;
			case 3:
				$this->displayMyParties();
				break;
			default:
				$this->notFound();
				break;
		}
	}

	public function displayFormCreationParty() {
		$app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = $_SERVER['DOCUMENT_ROOT'].str_replace("/index.php", "", $url) . '/doc/osm-montreal-rest.xml';
		$xmlControl = new Controller\XmlController($url);
		$xmlControl->analyse();
		$restaurants = $xmlControl->getRestaurants();
		$bars = $xmlControl->getBars();
		$nightClubs = $xmlControl->getNightClubs();

		$selectPlacesBar = '<select name="etape_bar_1_1" class="browser-default">';
		foreach ($bars as $bar) {
			$selectPlacesBar .= '<option value="'.$bar->name.'">'.$bar->name.'</option>';
		}
		$selectPlacesBar .= '</select>';

		$selectPlacesRestaurant = '<select name="etape_resto_1_1" class="browser-default">';
		foreach ($restaurants as $restaurant) {
			$selectPlacesRestaurant .= '<option value="'.$restaurant->name.'">'.$restaurant->name.'</option>';
		}
		$selectPlacesRestaurant .= '</select>';

		$selectPlacesClubs = '<select name="etape_club_1_1" class="browser-default">';
		foreach ($nightClubs as $nightClub) {
			$selectPlacesClubs .= '<option value="'.$nightClub->name.'">'.$nightClub->name.'</option>';
		}
		$selectPlacesClubs .= '</select>';

        $content = <<<END

        <h1 id="primary-title">Créer une nouvelle soiree</h1>
<div class="container">
	<form class="col s6 m6 l6" action="/creation">
		<div class="party-info">
			<div class="row">
				<div class="input-field col s6 m6 l6 offset-s3 offset-m3 offset-l3">
					<i class="material-icons prefix">create</i>
					<input id="icon_prefix" name="name" type="text" class="validate" required autofocus>
					<label for="icon_prefix">Nom de la soirée</label>
				</div>
				<div class="input-field col s6 m6 l6 offset-s3 offset-m3 offset-l3">
					<i class="material-icons prefix">today</i>
					<input type="date" class="datepicker" name="date" placeholder="Choisir une date" required>
				</div>
			</div>
		</div>
	<div id="config">
		<div id="party-configuration_1" class="row">
			<h3 class="center-align">Première configuration</h3>
			<h5 class="center-align">Première étape</h5>
			 <div class="input-field col s12">
				<select name="type_etape_1" class="browser-default" required>
					<option value="apero" selected>Apéro</option>
					<option value="restaurant">Restaurant</option>
					<option value="after">After</option>
				</select>
			</div>
			<div class="input-field col s12" id="select_event_1_1">
				$selectPlacesClubs
			</div>
			<div class="col s12">
				<div class="col s3">
					<h5>Heure de début</h5>
				</div>
				<div class="col s3">
					<input id="heure_deb_etap_1_1" name="heure_deb_etap_1_1" type="number" min="0" max="24" required>
				</div>
				<div class="col s1">
					<h5>:</h5>
				</div>
				<div class="col s3">
					<input id="min_deb_etap_1_1" name="min_deb_etap_1_1" type="number" min="0" max="60" required>
				</div>
			</div>
			<div class="col s12">
				<div class="col s3">
					<h5>Heure de fin</h5>
				</div>
				<div class="col s3">
					<input id="heure_fin_etap_1_1" name="heure_fin_etap_1_1" type="number" min="0" max="24" required>
				</div>
				<div class="col s1">
					<h5>:</h5>
				</div>
				<div class="col s3">
					<input id="min_fin_etap_1" name="min_fin_etap_1" type="number" min="0" max="60" required>
				</div>
			</div>
			<h5 class="center-align">Deuxième étape</h5>
			 <div class="input-field col s12">
				<select name="type_etape_1" class="browser-default">
					<option value="apero">Apéro</option>
					<option value="restaurant" selected>Restaurant</option>
					<option value="after">After</option>
				</select>
			</div>
			<div class="input-field col s12" id="select_event_1_2">
				$selectPlacesClubs
			</div>
			<div class="col s12">
				<div class="col s3">
					<h5>Heure de début</h5>
				</div>
				<div class="col s3">
					<input id="heure_deb_etap_1_2" name="heure_deb_etap_1_2" type="number" min="0" max="24">
				</div>
				<div class="col s1">
					<h5>:</h5>
				</div>
				<div class="col s3">
					<input id="min_deb_etap_1_2" name="min_deb_etap_1_2" type="number" min="0" max="60">
				</div>
			</div>
			<div class="col s12">
				<div class="col s3">
					<h5>Heure de fin</h5>
				</div>
				<div class="col s3">
					<input id="heure_fin_etap_1_2" name="heure_fin_etap_1_2" type="number" min="0" max="24">
				</div>
				<div class="col s1">
					<h5>:</h5>
				</div>
				<div class="col s3">
					<input id="min_fin_etap_1_2" name="min_fin_etap_1_2" type="number" min="0" max="60">
				</div>
			</div>
			<h5 class="center-align">Troisième étape</h5>
			 <div class="input-field col s12">
				<select name="type_etape_1" class="browser-default">
					<option value="apero">Apéro</option>
					<option value="restaurant">Restaurant</option>
					<option value="after" selected>After</option>
				</select>
			</div>
			<div class="input-field col s12" id="select_event_1_3">
				$selectPlacesClubs
			</div>
			<div class="col s12">
				<div class="col s3">
					<h5>Heure de début</h5>
				</div>
				<div class="col s3">
					<input id="heure_deb_etap_1_3" name="heure_deb_etap_1_3" type="number" min="0" max="24">
				</div>
				<div class="col s1">
					<h5>:</h5>
				</div>
				<div class="col s3">
					<input id="min_deb_etap_1_3" name="min_deb_etap_1_3" type="number" min="0" max="60">
				</div>
			</div>
			<div class="col s12">
				<div class="col s3">
					<h5>Heure de fin</h5>
				</div>
				<div class="col s3">
					<input id="heure_fin_etap_1_3" name="heure_fin_etap_1_3" type="number" min="0" max="24">
				</div>
				<div class="col s1">
					<h5>:</h5>
				</div>
				<div class="col s3">
					<input id="min_fin_etap_1_3" name="min_fin_etap_1_3" type="number" min="0" max="60">
				</div>
			</div>
		</div>

	</div>

		<div class="row">
			<a data-val="1" id="aConf" onclick="addConf()" class="waves-effect waves-light btn col s4 m4 l4 offset-s4 offset-m4 offset-l4 red darken-4" id="btn_new_step"><i class="material-icons left">add</i> Ajoutez une configuration <i class="material-icons right">add</i></a>
		</div>
		
		<div class="input-field col s6 m6 l6 offset-s3 offset-m3 offset-l3">
			<h5>Date de début du vote</h5>
			<i class="material-icons prefix">today</i>
			<input type="date" class="datepicker" name="dateVoteDeb" placeholder="Date de début du vote" required>
		</div>

		<div class="input-field col s6 m6 l6 offset-s3 offset-m3 offset-l3">
			<h5>Date de fin du vote</h5>
			<i class="material-icons prefix">today</i>
			<input type="date" class="datepicker" name="dateVoteFin" placeholder="Date de fin du vote" required>
		</div>

		<div class="col s12">
			<h5>Note éventuelle :</h5>
			<textarea name="note" id="note"></textarea>
		</div>
	
		<div class="center-align">
			<a class="waves-effect waves-light btn col s4 m4 l4 offset-s4 offset-m4 offset-l4 red darken-4" type="submit">Créer</a>
		</div>
	</form>
</div>
END;
		echo $content;
	}

	public function notFound() {
		echo "404 NOT FOUND";
	}

	public function displayApplications() {
		echo "applications";
	}

	public function displayMyParties() {

		$app = \Slim\Slim::getInstance();
		$url = $app->request->getRootUri();
		$url = str_replace("/index.php", "", $url);
		$i = 1;

		$html = '<h1 id="primary-title">Vos soirées</h1>';

		foreach($this->parties as $party) {

				 $html .= '<div class="container">
							<div class="card">
								<div class="card-image waves-effect waves-block waves-light">
								  <img class="activator" src="' . $url . '/web/img/bar.jpg">
								</div>
								<div class="card-content">
								  <span class="card-title activator grey-text text-darken-4">' . $party->name . '<i class="material-icons right">more_vert</i></span>
								</div>
								<div class="card-reveal">';

			foreach($party['configurations'] as $conf) {

				$html .= '<span class="card-title grey-text text-darken-4">Configuration n°' . $i . '<i class="material-icons right">close</i></span>
						  <p>';

				foreach($conf['events'] as $event) {

					$html .= '<b>Heure de départ : </b>' . $event['StartDate'] . '<br>
						  <b>Heure de fin : </b>' . $event['EndDate'] . '<br>
						  <b>Lieu : </b>' . $event['location'] . '<br>
						  <b>Type de l\'étape : </b>' . $event['type'] .'<br>
						  <hr>';
				}

				$html .= '</p>';
				$i++;
			}
			
			$html .= '</div></div></div>';

			echo $html;
			
		}

	}

}