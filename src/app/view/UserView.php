<?php

namespace app\view;

class UserView extends AbstractView
{

    private $message;
    private $parties;

    public function __construct($m,$part) {
        $this->message = $m;
        $this->parties = $part;
    }

    public function render($selector)
    {
        switch ($selector) {
            case 1:
                echo $this->printSignInForm();
                break;
            case 2:
                echo $this->printLoginForm();
                break;
            case 3:
                echo $this->printParties();
                break;
            default:
                echo $this->notFound();
                break;
        }
    }

    /*email name surname password avatar*/
    public function printSignInForm()
    {
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);
        $html = <<< END
<div class="container">
<h2 class="center-align teal-text">S'inscrire</h2>

<form class="col s12" method="POST" action="$url/index.php/signin" onsubmit="return verifyPassword()">
    <div class="row">
        <div class="input-field col l6 m6 s12 offset-l3">
            <i class="material-icons prefix teal-text">account_box</i>
            <input id="name" type="text" name="name" title="Nom" required>
            <label for="name" class="active">Nom</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col l6 m6 s12 offset-l3">
            <i class="material-icons prefix teal-text">account_circle</i>
            <input id="surname" type="text" name="surname" title="Prénom" required>
            <label for="surname" class="active">Prénom</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col l6 m12 s12 offset-l3">
            <i class="material-icons prefix teal-text">email</i>
            <input id="email" type="email" name="email" title="Email" required>
            <label for="email" class="active">Adresse mail</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col l6 m6 s12 offset-l3">
            <i class="material-icons prefix teal-text">lock</i>
            <input id="password" type="password" name="password" title="Mot de passe" required>
            <label for="password" class="active">Mot de passe</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col l6 m6 s12 offset-l3">
            <i class="material-icons prefix teal-text">lock</i>
            <input id="confirm_password" type="password" name="confirm_password" title="Confirmation de mot de passe" onblur="verifyPassword()" required>
            <label for="confirm_password" class="active">Confirmation du mot de passe</label>
        </div>
    </div>
    <div class="row">
        <button class="waves-effect waves-light btn col l2 m2 s12 offset-l5 offset-m5" type="submit" name="validation" value="validation"><i>S'inscrire</i></button>
    </div>
    <br><br>
</form>
</div>
END;

        if($this->message != null) {
            $html .= '<script>
						document.addEventListener("DOMContentLoaded",function() {
							Materialize.toast("' . $this->message['message'] . '", 4000);
						});
					  </script>';
        }

        return $html;
    }

    public function printLoginForm()
    {
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);
        $html = <<< END

	<h2 class="center-align teal-text">Connexion</h2>

	<form class="col s12" method="POST" action="$url/login">
		<div class="row">
			<div class="input-field col s12 m4 l4 offset-l4 offset-m4">
				<input type="text" name="email"  required>
				<label class="active">Adresse mail</label>
			</div>
			<div class="input-field col s12 m4 l4 offset-l4 offset-m4">
				<input type="password" name="password" required>
				<label class="active">Mot de passe</label>
			</div>
		</div>	
		<div class="row">
			<button class="waves-effect waves-light btn col l2 m2 s12 offset-l5 offset-m5 teal" type="submit" name="login" value="login"><i>Se connecter</i></button> 
		</div>
		<br><br><br><br><br><br><br>
	</form>

END;
        return $html;
    }

    public function printParties() {

        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);
        $i = 1;

        $html = '<h1 id="primary-title">Liste des soirées</h1>';

        foreach($this->parties as $party) {

            $html .= '<div class="container">
							<div class="card">
								<div class="card-image waves-effect waves-block waves-light">
								  <img class="activator" src="' . $url . '/web/img/bar.jpg">
								</div>
								<div class="card-content">
								  <span class="card-title activator grey-text text-darken-4">' . $party->name . '<i class="material-icons right">more_vert</i></span>
								</div>
								<div class="card-reveal">';

            foreach($party['configurations'] as $conf) {

                $html .= '<span class="card-title grey-text text-darken-4">Configuration n°' . $i . '<i class="material-icons right">close</i></span>
						  <table><tr>';

                foreach($conf['events'] as $event) {

                    $html .= '<td><p></p><b>Heure de départ : </b>' . $event['StartDate'] . '<br>
						  <b>Heure de fin : </b>' . $event['EndDate'] . '<br>
						  <b>Lieu : </b>' . $event['location'] . '<br>
						  <b>Type de l\'étape : </b>' . $event['type'] .'<br>
						  </p></td>';
                }

                $html .= '</tr></table><hr>';
                $i++;
            }

            $html .= '</div></div></div>';

            echo $html;

        }
    }
}