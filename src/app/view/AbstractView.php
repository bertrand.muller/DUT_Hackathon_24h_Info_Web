<?php

namespace app\view;

abstract class AbstractView {

    public function renderHeader() {

        include('web/html/header.php');

        $html = '<nav class="red darken-4" role="navigation">
                    <div class="nav-wrapper container">
                        <a id="logo-container" href="home" class="white-text brand-logo">IUT Montreal Events</a>
                        <ul class="right hide-on-med-and-down">';

        if(isset($_SESSION['connected'])) {
            if ($_SESSION['connected'] == true) {

                if (isset($_SESSION['auth_level'])) {
                    if ($_SESSION['auth_level'] == 15) {
                        $html .= '<li><a class="white-text" href="home">Accueil</a></li>
                                  <li><a class="white-text" href="accept">Gérer les membres</a></li>
                                  <li><a class="white-text" href="myParties">Mes Soirées</a></li>
                                  <li><a class="white-text" href="creation">Création d\'une soirée</a></li>';
                    } else {
                        $html .= '<li><a class="white-text" href="home">Accueil</a></li>
                                  <li><a class="white-text" href="parties">Soirées</a></li>';
                    }
                }
				$html .= '<li><a class="white-text" href="logout">Se déconnecter</a></li>';

            } else {
                $html .= '<li><a class="white-text" href="home">Accueil</a></li>
                          <li><a class="white-text" href="signin">Inscription</a></li>
                          <li><a class="white-text" href="login">Connexion</a></li>';
            }

        } else {
                $html .= '<li><a class="white-text" href="home">Accueil</a></li>
                          <li><a class="white-text" href="signin">Inscription</a></li>
                          <li><a class="white-text" href="login">Connexion</a></li>';
        }

       $html .= '</ul>
                 <ul id="nav-mobile" class="side-nav red darken-4">';

        if(isset($_SESSION['connected'])) {
            if ($_SESSION['connected'] == true) {

                if (isset($_SESSION['auth_level'])) {
                    if ($_SESSION['auth_level'] == 15) {
                        $html .= '<li><a class="white-text" href="home">Accueil</a></li>
                                  <li><a class="white-text" href="myParties">Mes Soirées</a></li>';
                    } else {
                        $html .= '<li><a class="white-text" href="home">Accueil</a></li>
                                  <li><a class="white-text" href="parties">Soirées</a></li>';
                    }
                }

            } else {
                $html .= '<li><a class="white-text" href="home">Accueil</a></li>
                          <li><a class="white-text" href="signin">Inscription</a></li>
                          <li><a class="white-text" href="login">Connexion</a></li>';
            }

        } else {
            $html .= '<li><a class="white-text" href="home">Accueil</a></li>
                      <li><a class="white-text" href="signin">Inscription</a></li>
                      <li><a class="white-text" href="login">Connexion</a></li>';
        }

        $html .= '</ul>
                    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons white-text">menu</i></a>
                </div>
            </nav>';

        echo $html;

    }


    public function renderFooter() {
        include('web/html/footer.php');
    }


    public function globalRender($selector) {
        $this->renderHeader();
        $this->render($selector);
        $this->renderFooter();
    }

    public abstract function render($selector);

}