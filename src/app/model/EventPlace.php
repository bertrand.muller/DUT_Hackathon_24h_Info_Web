<?php

namespace app\model;

abstract class EventPlace {

	public $latitude;
	public $longitude;
	public $name;
	public $website;
	public $address_number;
	public $address_street;
	public $address_post_code;
	public $opening_hours;

	public function __construct($lat, $long, $na)
	{
		$this->latitude = $lat;
		$this->longitude = $long;
		$this->name = $na;
	}

}