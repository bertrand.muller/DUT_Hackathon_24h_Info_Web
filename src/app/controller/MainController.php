<?php

namespace app\controller;

use app\view as View;

class MainController extends AbstractController {

	public function getHome() {
		$view = new View\HomeView(null);
		$view->globalRender(1);
	}

	public function getNotFound()
	{
		$view = new View\HomeView(null);
		$view->globalRender(2);
	}

}