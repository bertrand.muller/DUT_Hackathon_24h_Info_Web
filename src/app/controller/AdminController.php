<?php

namespace app\controller;

use app\view as View;
use conf as Conf;
use app\model as Model;

class AdminController extends AbstractController
{

    public function getCreationParty()
    {
        try {
            Conf\Authentication::checkSession();
            Conf\Authentication::checkAccessRights(1);
            $view = new View\AdminView();
            $view->globalRender(1);
        } catch (Conf\AuthException $e) {
            $view = new View\HomeView(array('message' => 'Vous n\'avez pas les droits pour créer une soirée.'));
            $view->globalRender(1);
        }
    }

    public function getNotFound()
    {
        $view = new View\AdminView();
        $view->globalRender(0);
    }

    public function getApplications()
    {
        try {
            Conf\Authentication::checkSession();
            Conf\Authentication::checkAccessRights(1);
            $applications = Model\User::where('accepted', '=', '0')->get()->toArray();
            $view = new View\AdminView($applications);
            $view->globalRender(2);
        } catch (Conf\AuthException $e) {
            $view = new View\HomeView(array('message' => 'Vous n\'avez pas les droits pour accèder aux demandes d\'inscriptions en attente d\'approbation.'));
            $view->globalRender(1);
        }
    }

    public function getMyParties()
    {
        try {
            Conf\Authentication::checkSession();
            Conf\Authentication::checkAccessRights(15);
            $parties = Model\Party::all();
            foreach ($parties as $key => $party) {
                $configurations = Model\Configuration::where('partyId', '=', $party->id)->get()->toArray();

                foreach ($configurations as $keyConf => $conf) {
                    $events = Model\Event::where('configurationId', '=', $conf['id'])->get()->toArray();
                    $configurations[$keyConf]['events'] = $events;
                    $votes = Model\Vote::where('configurationId', '=', $conf['id'])->get()->toArray();
                    $configurations[$keyConf]['votes'] = $votes;
                }
                $parties[$key]['configurations'] = $configurations;
            }
            $view = new View\AdminView($parties);
            $view->globalRender(3);
        } catch (Conf\AuthException $e) {
            $view = new View\HomeView(array('message' => 'Vous n\'avez pas les droits pour accèder aux soirées crées par un administrateur.'));
            $view->globalRender(1);
        }
    }

    public function acceptUser($id)
    {
        try {
            Conf\Authentication::checkSession();
            Conf\Authentication::checkAccessRights(15);
            $user = Model\User::find($id);
            if ($user['accepted'] == 0) {
                $user['accepted'] = 1;
                $user->save();
            }
            // TODO redirect avec message
        } catch (Conf\AuthException $e) {
            $view = new View\HomeView(array('message' => 'Vous n\'avez pas les droits pour accepter l\'inscription d\'un utilisateur.'));
            $view->globalRender(1);
        }
    }

    public function rejectUser($id)
    {
        try {
            Conf\Authentication::checkSession();
            Conf\Authentication::checkAccessRights(15);
            $user = Model\User::find($id);
            if ($user->accepted == 0) {
                $user->delete();
                $mess = 'Utilisateur supprimé.';
            }
            // TODO redirect avec message
        } catch (Conf\AuthException $e) {
            $view = new View\HomeView(array('message' => 'Vous n\'avez pas les droits pour rejeter la demande d\'inscription d\'un utilisateur.'));
            $view->globalRender(1);
        }
    }

    public function checkAndValidateUser()
    {
        try {
            Conf\Authentication::checkAccessRights(15);
            $app = \Slim\Slim::getInstance();
            $url = $app->request->getRootUri();
            $url = str_replace("/index.php", "", $url);
            $draw = "<form action='$url/accept' method='post'>";
            $users = Model\User::where('accepted', 0)->get()->toArray();
            foreach ($users as $user) {
                $draw .= "<tr>";
                $draw .= "<td>" . $user['email'] . "</td>";
                $draw .= "<td>" . $user['name'] . "</td>";
                $draw .= "<td>" . $user['surname'] . "</td>";
                $draw .= "<td><button class='waves-effect waves-light btn green darken-4' type='submit' name='validate' value='" . $user['id'] . "'><i class='material-icons tiny'>thumb_up</i></button></td>";
                $draw .= "<td><button class='waves-effect waves-light btn red darken-4' type='submit' name='refuse' value='" . $user['id'] . "'><i class='material-icons tiny'>thumb_down</i></button></td>";
                $draw .= "</tr>";
            }
            $draw .= "</form>";
            $view = new View\AdminView($draw);
            $view->globalRender(4);
        } catch (Conf\AuthException $e) {
            $view = new View\HomeView(array('message' => 'Vous n\'avez pas les droits pour cette action.'));
            $view->globalRender(1);
        }

    }


	public function postCreation($post){


		$part = new Model\Party();
		$part->name = $post['name'];
		$part->date = new \DateTime($post['date']);
		$part->save();




		$arret = true;
		$i = 0;

		while($arret){




			$conf = new Model\Configuration();
			$conf->id = $part->id;

			$conf->StartVoteTime = $post['dateVoteDeb'];
			$conf->EndVoteTime = $post['dateVoteFin'];

			if(isset($post['note'])){


				$conf->description = $post['note'];


			}

			$arret = false;
		}



	}

}