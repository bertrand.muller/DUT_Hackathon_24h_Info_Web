<?php 

namespace app\controller;

use app\model as Model;
use conf as Conf;

class XmlController {
	
	private $xml;
	private $xmlObject;
	private $l_restaurants;
	private $l_bars;
	private $l_nightclubs;

	public function __construct($file)
	{
		$this->xml = $file;
		$this->xmlObject = simplexml_load_file($this->xml);
	}

	public function analyse()
	{
		$this->l_restaurants = array();
		$this->l_bars = array();
		$this->l_nightclubs = array();
		foreach ($this->xmlObject->node as $node) {
			$tags = $node->tag;
			foreach ($tags as $tag) {
				switch ($tag['k']) {
					case 'amenity':
						$amenity = $tag['v'];
						break;
					case 'name':
						$name = $tag['v'];
						break;
					case 'website':
						$amenity = $tag['v'];
						break;
					case 'cuisine':
						$cooking = $tag['v'];
						break;
					case 'addr:housenumber':
						$add_number = $tag['v'];
						break;
					case 'addr:street':
						$add_street = $tag['v'];
						break;
					case 'addr:postcode':
						$add_postcode = $tag['v'];
						break;
					case 'opening_hours':
						$open = $tag['v'];
					case 'amenity':
						$amenity = $tag['v'];
						break;
					default:
						break;
				}
			}
			
			switch ($amenity) {
				case 'restaurant':
					$eventPlace = new Model\Restaurant($node['lat'], $node['lon'], $name);
					if (isset($cooking))
						$eventPlace->cookType = $cooking;
					if (isset($website))
					$eventPlace->website = $website;
					if (isset($add_number))
						$eventPlace->address_number = $add_number;
					if (isset($add_street))
						$eventPlace->address_street = $add_street;
					if (isset($add_postcode))
						$eventPlace->address_postcode = $add_postcode;
					if (isset($open))
						$eventPlace->opening_hours = $open;
					$this->l_restaurants[] = $eventPlace;
					break;
				case 'bar':
					$eventPlace = new Model\Bar($node['lat'], $node['lon'], $name);
					if (isset($website))
					$eventPlace->website = $website;
					if (isset($add_number))
						$eventPlace->address_number = $add_number;
					if (isset($add_street))
						$eventPlace->address_street = $add_street;
					if (isset($add_postcode))
						$eventPlace->address_postcode = $add_postcode;
					if (isset($open))
						$eventPlace->opening_hours = $open;
					$this->l_bars[] = $eventPlace;
					break;
				case 'nightclub':
					$eventPlace = new Model\NightClub($node['lat'], $node['lon'], $name);
					if (isset($website))
					$eventPlace->website = $website;
					if (isset($add_number))
						$eventPlace->address_number = $add_number;
					if (isset($add_street))
						$eventPlace->address_street = $add_street;
					if (isset($add_postcode))
						$eventPlace->address_postcode = $add_postcode;
					if (isset($open))
						$eventPlace->opening_hours = $open;
					$this->l_nightclubs[] = $eventPlace;
					break;
				default:
					break;
			}
			
		}
	}

	public function getRestaurants()
	{
		return $this->l_restaurants;
	}

	public function getBars()
	{
		return $this->l_bars;
	}
	public function getNightclubs()
	{
		return $this->l_nightclubs;
	}

}