<?php

namespace app\controller;

use app\model as Model;
use app\model\Event;
use app\view as View;
use \conf as Conf;

class UserController extends AbstractController
{

    function getSignInForm()
    {
        if (!isset($_SESSION['connected']) || !$_SESSION['connected']) {
            $userView = new View\UserView(null,null);
            $userView->globalRender(1);
        }
        else   
        {
            $app = \Slim\Slim::getInstance();
            $url = $app->request->getRootUri();
            $url = str_replace("/index.php", "", $url); 
            header('Location: '.$url);
            exit();
        }  
    }

    function getLoginForm()
    {
        if (!isset($_SESSION['connected']) || !$_SESSION['connected']) {
            $userView = new View\UserView(null,null);
            $userView->globalRender(2);
        }
        else
        {
            $app = \Slim\Slim::getInstance();
            $url = $app->request->getRootUri();
            $url = str_replace("/index.php", "", $url); 
            header('Location: '.$url);
            exit();
        }
    }

    function postSignIn($post)
    {
        $app = \Slim\Slim::getInstance();
        $params = $app->request->post();
        $nom = filter_var($params['name'], FILTER_SANITIZE_STRING);
        $prenom = filter_var($params['surname'], FILTER_SANITIZE_STRING);
        $email = filter_var($params['email'], FILTER_SANITIZE_EMAIL);
        $mdp = $params['password'];

        try {
            Conf\Authentication::createUser($email, 1, $mdp, $nom, $prenom);
            /*$vue = new View\VueUtilisateur(array('email' => $email, 'nom' => $nom , 'prenom' => $prenom , 'pseudo' => $pseudo));
            $vue->render(3);*/
            $app = \Slim\Slim::getInstance();
            $url = $app->request->getRootUri();
            $url = str_replace("/index.php", "", $url); 
            header('Location: '.$url.'/login');
            exit();
        } catch (Conf\AuthException $e) {
            /*$vue = new View\VueErreurs(array($e->getMessage()));
            $vue->render(3);*/
            $view = new View\UserView(array('message'=>'Création impossible, veuillez réessayer.'),null);
            $view->globalRender(1);
        }
    }

    function postLogin($post)
    {
        if (!$_SESSION['connected']) {
            $app = \Slim\Slim::getInstance();
            $params = $app->request->post();

            $email = $params['email'];
            $mdp = $params['password'];

            $email = filter_var($email, FILTER_SANITIZE_EMAIL);

            try {

                Conf\Authentication::authenticate($email, $mdp);
                Conf\Authentication::loadProfil($email);
                $vue = new View\HomeView(null);
                $vue->globalRender(1);
            } catch (Conf\AuthException $ae) {
                //$vue = new View\VueErreurs(array($ae->getMessage()));
                //$vue->render(2);
                $view = new View\UserView(array('message'=>'Connexion impossible, le mot de passe ou l\'email ne correspond pas.'),null);
                $view->globalRender(2);
            }
        } else {
            $vue = new View\UserView(null);
            $vue->globalRender(1);
        }
    }

    function getParties()
    {
        try {
            Conf\Authentication::checkSession();
            Conf\Authentication::checkAccessRights(5);
            $parties = Model\Party::all();
            foreach ($parties as $key => $party) {
                $configurations = Model\Configuration::where('partyId','=',$party->id)->get()->toArray();
                
                foreach ($configurations as $keyConf => $conf) {
                    $events = Model\Event::where('configurationId','=',$conf['id'])->get()->toArray();
                    $configurations[$keyConf]['events'] = $events;
                    $votes = Model\Vote::where('configurationId','=',$conf['id'])->get()->toArray();
                    $configurations[$keyConf]['votes'] = $votes;
                }
                $parties[$key]['configurations'] = $configurations;
            }
            $userView = new View\UserView(null,$parties);
            $userView->globalRender(3);
        }
        catch (Conf\AuthException $e)
        {
           $view = new View\HomeView(array('message'=>'Vous n\'avez pas les droits pour accéder aux soirées.'));
            $view->globalRender(1); 
        }
    }

    function vote()
    {
        try {
            Conf\Authentication::checkSession();
            Conf\Authentication::checkAccessRights(0);
            $app = \Slim\Slim::getInstance();
            $post = $app->request->post();
            $confId = filter_var($post['configurationId'], FILTER_SANITIZE_NUMBER_INT);
            $userId = filter_var($post['userId'], FILTER_SANITIZE_NUMBER_INT);
            $vote = Model\Vote::where('userId','=',$userId)->where('configurationId','=',$confId)->get()->toArray();
            if (count($vote) > 0)
            {
                $mess = 'Vous avez déjà voté pour cette soirée.';
            }
            else
            {
                $vote = new Vote();
                $vote->userId=$userId;
                $vote->configurationId = $confId;
                $vote->date = new DateTime();
                $vote->save();
                $mess = 'Voté !';
            }
            $view = new View\UserView(array('message' => $mess),null);
            $view->globalRender(3);
        }
        catch (Exception $e)
        {
            $view = new View\HomeView(array('message'=>'Vous n\'avez pas les droits pour voter.'));
            $view->globalRender(1);
        }
    }

    public function DEBUG()
    {
//        var_dump($_SERVER);
//        $url = $_SERVER['DOCUMENT_ROOT']
////        $debug = \app\model\Event::find(1)->toArray();
//        $debug = Model\Event::find(1)->toArray();
//        $number = count($debug);
//        $monics = "BEGIN:VCALENDAR".chr(10); // chr(10) code ascii du saut de ligne
//        $monics = $monics."VERSION:2.0".chr(10);
//        $monics = $monics."PRODID:-//hacksw/handcal//NONSGML v1.0//EN".chr(10);
//                $monics = $monics."BEGIN:VEVENT".chr(10);
//                // date au bon format
//                $monics = $monics."DTSTART:".$debug['StartDate'].chr(10);
//                    $monics = $monics."DTEND:".$debug['EndDate'].chr(10);
//                $monics = $monics."SUMMARY:Anniversaire de MDR".chr(10);
//                $monics = $monics."LOCATION:Parlement bruxellois".chr(10);
//                $monics = $monics."DESCRIPTION: TONCUL".chr(10);
//                $monics = $monics."END:VEVENT".chr(10);
//            $monics = $monics."END:VCALENDAR".chr(10);
//            // écrire dans fichier
//            $ficin="\"".$url."\\test.ics\"";
//            $monfic = fopen($ficin, "w");
//            fwrite($monfic, $monics);
//            fclose($monfic);
//            echo "Télécharger au format <a href=\"".$url."/intranet/anniversaire/anniversaire.ics\">iCalendar</a>";
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=testMax.ics');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        $debug = \app\model\Event::find(1)->toArray();
        // chr(10) code ascii du saut de ligne
        $content = "BEGIN:VCALENDAR".chr(10);
        $content .= "VERSION:2.0".chr(10);
        $content .= "PRODID:-//hacksw/handcal//NONSGML v1.0//EN".chr(10);
        $content .= "BEGIN:VEVENT".chr(10);
        $content .= "DTSTART:".$debug['StartDate'].chr(10);
        $content .= "DTEND:".$debug['EndDate'].chr(10);
        $content .= "SUMMARY:Anniversaire de MDR".chr(10);
        $content .= "LOCATION:Parlement bruxellois".chr(10);
        $content .= "DESCRIPTION: TONCUL".chr(10);
        $content .= "END:VEVENT".chr(10);
        $content .= "END:VCALENDAR".chr(10);
        header('Content-Length: ' . strlen($content));
        ob_clean();
        flush();
        echo $content;
        exit;
    }

    function logOut()
    {
        session_destroy();
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url); 
        header('Location: '.$url.'/login');
        exit();
    }

}